<?php

namespace Drupal\uikit_grid_layouts;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\file\Entity\File;

/**
 * Extends LayoutDefault Class.
 */
class UikitGridLayoutsClass extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'section_background' => 'uk-section-default',
      'section_background_image' => '',
      'section_background_image_url' => '',
      'section_padding' => '',
      'container_width' => '',
      'column_width' => '',
      'grid_gutter' => 'uk-grid-medium',
      'grid_divider' => 'false',
      'grid_column_position' => 'uk-flex-center',
      'card' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();

    // Section background.
    $section_background_options = [
      'uk-section-default' => $this->t('Default'),
      'uk-section-muted' => $this->t('Muted'),
      'uk-section-primary' => $this->t('Primary'),
      'uk-section-secondary' => $this->t('Secondary'),
    ];

    $form['section_background'] = [
      '#title' => $this->t('Section background'),
      '#type' => 'select',
      '#default_value' => $configuration['section_background'],
      '#options' => $section_background_options,
    ];

    // Section background image.
    $form['section_background_image'] = [
      '#title' => t('Upload an image file for section background'),
      '#type' => 'managed_file',
      '#upload_location' => 'public://background/',
      '#multiple' => FALSE,
      '#default_value' => $configuration['section_background_image'],
      '#description' => t('Allowed extensions: gif png jpg jpeg'),
      '#upload_validators' => [
        'file_validate_is_image' => [],
        'file_validate_extensions' => ['gif png jpg jpeg'],
        'file_validate_size' => [25600000],
      ],
    ];

    // Section padding.
    $section_padding_options = [
      'uk-padding-remove-vertical' => $this->t('No vertical padding'),
      'uk-section-xsmall' => $this->t('Xsmall'),
      'uk-section-small' => $this->t('Small'),
      'uk-section-large' => $this->t('Large'),
      'uk-section-xlarge' => $this->t('Xlarge'),
    ];

    $form['section_padding'] = [
      '#title' => $this->t('Section padding'),
      '#type' => 'select',
      '#empty_option' => $this->t('-Default-'),
      '#default_value' => $configuration['section_padding'],
      '#options' => $section_padding_options,
    ];

    // Container width.
    $container_width_options = [
      'uk-container-xsmall' => $this->t('Xsmall'),
      'uk-container-small' => $this->t('Small'),
      'uk-container-large' => $this->t('Large'),
      'uk-container-xlarge' => $this->t('Xlarge'),
      'uk-container-expand' => $this->t('No limit'),
    ];

    $form['container_width'] = [
      '#title' => $this->t('Container width'),
      '#type' => 'select',
      '#empty_option' => $this->t('-Default-'),
      '#default_value' => $configuration['container_width'],
      '#options' => $container_width_options,
    ];

    // Grid Gutter.
    $grid_gutter_options = [
      'uk-grid-small' => $this->t('Small'),
      'uk-grid-medium' => $this->t('Medium'),
      'uk-grid-large' => $this->t('Large'),
      'uk-grid-collapse' => $this->t('Remove the gutter'),
    ];

    $form['grid_gutter'] = [
      '#title' => $this->t('Grid Gutter size'),
      '#type' => 'select',
      '#empty_option' => $this->t('-None-'),
      '#default_value' => $configuration['grid_gutter'],
      '#options' => $grid_gutter_options,
    ];

    $form['grid_divider'] = [
      '#title' => $this->t('Divider modifier'),
      '#type' => 'checkbox',
      '#default_value' => $configuration['grid_divider'],
      '#description' => t('Check if you want separate grid cells with lines'),
    ];

    $form['card'] = [
      '#title' => $this->t('Layout contain cards'),
      '#type' => 'checkbox',
      '#default_value' => $configuration['card'],
    ];

    // Specific to each template
    // Column size.
    $column_size_options = [];
    $layout_id = $this->getPluginDefinition()->id();

    $column_size_options['uikit_one_column'] = [
      '25' => '25%',
      '33' => '33%',
      '50' => '50%',
      '66' => '66%',
      '75' => '75%',
      '100' => '100%',
    ];

    $column_size_options['uikit_two_column'] = [
      '25/75' => '25/75',
      '33/66' => '33/66',
      '50/50' => '50/50',
      '66/33' => '66/33',
      '75/25' => '75/25',
    ];

    $column_size_options['uikit_three_column'] = [
      '33/33/33' => '33/33/33',
      '25/50/25' => '25/50/25',
      '25/25/50' => '25/25/50',
      '50/25/25' => '50/25/25',
    ];

    if ($layout_id != 'uikit_four_column') {
      $form['column_width'] = [
        '#title' => $this->t('Column size'),
        '#type' => 'select',
        '#default_value' => $configuration['column_width'],
        '#options' => $column_size_options[$layout_id],
      ];
    }

    // Grid Column position.
    $grid_column_position = [
      'uk-flex-left' => $this->t('Left'),
      'uk-flex-center' => $this->t('Center'),
      'uk-flex-right' => $this->t('Right'),
    ];

    $form['grid_column_position'] = [
      '#title' => $this->t('horizontal columns alignment'),
      '#type' => 'select',
      '#default_value' => $configuration['grid_column_position'],
      '#options' => $grid_column_position,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Any additional form validation that is required.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['section_background'] = $form_state->getValue('section_background');
    $this->configuration['section_background_image'] = $form_state->getValue('section_background_image');
    // Save image as permanent file and prepopulate image url to template.
    if (!empty($form_state->getValue('section_background_image'))) {
      $bg_img_fids = $form_state->getValue('section_background_image');
      $bg_img_fid = $bg_img_fids[0];
      $bg_img_file = File::load($bg_img_fid);
      $bg_img_file->setPermanent();
      $bg_img_file->save();
      \Drupal::service('file.usage')->add($bg_img_file, 'uikit_layouts', 'file', $bg_img_fid);
      $bg_img_path = $bg_img_file->getFileUri();
      $this->configuration['section_background_image_url'] = $image_path = \Drupal::service('file_url_generator')->generateAbsoluteString($bg_img_path);
    }
    else {
      $this->configuration['section_background_image_url'] = '';
    }
    $this->configuration['section_padding'] = $form_state->getValue('section_padding');
    $this->configuration['container_width'] = $form_state->getValue('container_width');
    $this->configuration['column_width'] = $form_state->getValue('column_width');
    $this->configuration['grid_gutter'] = $form_state->getValue('grid_gutter');
    $this->configuration['grid_divider'] = $form_state->getValue('grid_divider');
    $this->configuration['grid_column_position'] = $form_state->getValue('grid_column_position');
    $this->configuration['card'] = $form_state->getValue('card');
  }

}
